# Matrix Postman

Protonmail to Matrix mail parser.

## Development

### Configure protonBridge

1. Launch proton command line
```shell
make proton
```
2. Add account : type `login` and follow prompt
3. Type `info` and copy Username and password into a `.env` file
```env
IMAP_USER=<username>
IMAP_PASS=<password>
```
4. Type `exit`

### Start dev stack

```shell
make dev
```
