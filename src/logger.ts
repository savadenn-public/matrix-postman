import chalk from 'chalk'

export function log({
  message,
  prefix,
  data,
}: {
  message: string
  prefix?: string
  data?: unknown
}): void {
  const messages = []
  if (prefix) {
    messages.push(chalk.bold.green(prefix))
  }
  messages.push(message)
  if (data) {
    console.log(chalk.blue(messages.join(chalk.grey(' | '))), data)
  } else {
    console.log(chalk.blue(messages.join(chalk.grey(' | '))))
  }
}

export function alert({
  message,
  prefix,
}: {
  message: string
  prefix?: string
}): void {
  const messages = []
  if (prefix) {
    messages.push(chalk.bold.green(prefix))
  }
  messages.push(message)
  console.log(chalk.red(messages.join(chalk.grey(' | '))))
}
