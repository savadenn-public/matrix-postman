import '@/moment'
import { checkEnv } from '@/CheckEnv'
import { ImapFlow } from 'imapflow'

checkEnv()

const client = new ImapFlow({
  host: 'protonBridge',
  port: 143,
  secure: false,
  auth: {
    user: process.env.IMAP_USER,
    pass: process.env.IMAP_PASS,
  },
  tls: {
    rejectUnauthorized: false,
  },
})

interface ExistData {
  path: string
  count: number
  prevCount: number
}

client.on('exists', (data: ExistData) => {
  console.log(`Message count in "${data.path}" is ${data.count}`)
})

client.on('close', connect)

async function connect() {
  // Wait until client connects and authorizes
  await client.connect()

  // Select and lock a mailbox. Throws if mailbox does not exist
  const lock = await client.getMailboxLock('INBOX')
}

connect().catch((err) => console.error(err))
