const REQUIRED_VARIABLES = ['IMAP_USER', 'IMAP_PASS']

/**
 * Check all required env variables
 */
export function checkEnv(): void {
  let missingVariable = false
  REQUIRED_VARIABLES.forEach((variable) => {
    if (!process.env[variable]) {
      console.error(`Missing environment variable ${variable}`)
      missingVariable = true
    }
  })
  if (missingVariable) process.exit(1)
}
