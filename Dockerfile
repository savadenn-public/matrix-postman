FROM node:15

RUN wget -q \
    -O protonmail.deb \
    https://protonmail.com/download/bridge/protonmail-bridge_1.8.2-1_amd64.deb

RUN apt update -q \
    && dpkg -i protonmail.deb || apt install -fy \
    && rm -f protonmail.deb
