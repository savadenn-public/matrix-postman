#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY:
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_\-\0.0-9]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: ## Cleans up environnement
	@docker-compose down --remove-orphans
	@docker-compose pull || true


docker.build: clean ## Build docker image
	@docker-compose build --pull

dev: clean ## Starts dev stack
	@docker-compose up -d

dev.debug: clean ## Starts dev stack in debug mode
	@docker-compose -f ./docker-compose.yml -f ./docker-compose.debug.yml up -d

dev.send: ## Send mail to started dev serve
	@docker-compose run --rm bot sh -c "yarn run dev:send"

install: ## Starts dev stack with TDD
	@docker-compose run --rm bot yarn

sh: ## Open bash in node container
	@docker-compose run --rm bot bash

# ----------
# Proton connector
# ----------

proton: clean ## Initiates protonmail bridge
	@docker-compose run --rm protonBridge init

